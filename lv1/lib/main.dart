import 'package:flutter/material.dart';
import 'data.dart';
import 'data_card.dart';
import 'quote.dart';
import 'package:toast/toast.dart';
import 'dart:math';

void main() {
  runApp(MaterialApp(
    home: Persons(),
  ));
}

class Persons extends StatefulWidget {
  @override
  _PersonsState createState() => _PersonsState();
}


class _PersonsState extends State<Persons> {

  final TextEditingController controller = new TextEditingController();

  int _group = 0;

  List<Data> datas = [
    Data(author: "Mark Zuckerberg", text: "Be yourself; else is already taken Be yourself; everyone else is already taken Be yourself; else is already taken Be yourself; everyone else is already taken Be yourself; else is already taken Be yourself; everyone else is already taken Be yourself; else is already taken Be yourself; everyone else is already taken", year: "1984-now", image_url: "https://media.npr.org/assets/img/2018/11/21/gettyimages-962142720-3f4af695a639cbc14deb90e88287cd3c19b676f4-s800-c85.jpg"),
    Data(author: "Bill Gates", text: "I have nothing to declare except my genius I have nothing to declare except my genius", year: "1955-now", image_url: "https://pbs.twimg.com/profile_images/988775660163252226/XpgonN0X.jpg"),
    Data(author: "Edward Snowden", text: "The truth is rarely pure and never simple. Keep it simple. pure and never simple. Keep it simple. ", year: "1983-now", image_url: "https://i.guim.co.uk/img/media/53bcd9a5177086f5eaec8c9474b1879e05e35910/120_0_3600_2160/master/3600.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=842a98699b2bd72d3b3feb5753b8f60a")
  ];

  List<Quote> quotes = [
    Quote(author: "Mark Zuckerberg",
        quotes: ["I think a simple rule of business is, if you do the things that are easier first, then you can actually make a lot of progress.",
          "By giving people the power to share, we're making the world more transparent.",
          "The basis of our partnership strategy and our partnership approach: We build the social technology. They provide the music."]),
    Quote(author: "Bill Gates",
        quotes: ["Life is not fair; get used to it.",
          "Success is a lousy teacher. It seduces smart people into thinking they can't lose.",
          "Your most unhappy customers are your greatest source of learning."]),
    Quote(author: "Edward Snowden",
        quotes: ["Under observation, we act less free, which means we effectively are less free.",
          "Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say.",
          "Being called a traitor by Dick Cheney is the﻿ highest honor you can give to an American."]),
  ];

  void ShowToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  String RandomQuote(id) {
    Random random = new Random();
    int rand = random.nextInt(3);
    return quotes[id].quotes[rand];
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Famous Scientists"),
        centerTitle: true,
        backgroundColor: Colors.teal[300],
      ),
      backgroundColor: Colors.grey[200],
      body: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: ListView(
            children:[Container(
              child: Column(
                children: [ Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30.0),
                        child: FlatButton(
                          onPressed: () => ShowToast(RandomQuote(_group), gravity: Toast.BOTTOM, duration: 3),
                          color: Colors.grey[400],
                          child: Text("INSPIRATION"),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                        padding: const EdgeInsets.fromLTRB(0, 10.0, 0, 10.0),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(color: Colors.teal[300])
                          ),
                        ),
                        child: Center(child: Text(
                          "TOP 3",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                      ),
                    )
                  ],
                ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: datas.map((data) => PersonCard(
                        data: data
                    )).toList(),
                  ),
                  Form(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "Enter description",
                            ),
                            controller: controller,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                setState(() {
                                  if(controller.text.isEmpty) {
                                    ShowToast("Please enter some text!", gravity: Toast.CENTER, duration: 3);
                                  } else {
                                    datas[_group].text = controller.text;
                                  }
                                });
                                controller.text = "";
                              },
                              disabledColor: Colors.grey[400], // Change when add on pressed
                              child: Text(
                                "EDIT DESCRIPTION",
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: width,
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Radio(
                                      value: 0,
                                      groupValue: _group,
                                      onChanged: (value) {
                                        setState(() {
                                          _group = value;
                                        });
                                      },
                                    ),
                                    Container(
                                      width: width*0.2,
                                      child: Text(
                                        "First person",
                                        style: TextStyle(
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Radio(
                                      value: 1,
                                      groupValue: _group,
                                      onChanged: (value) {
                                        setState(() {
                                          _group = value;
                                        });
                                      },
                                    ),
                                    Container(
                                      width: width*0.2,
                                      child: Text(
                                        "Second person",
                                        style: TextStyle(
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Radio(
                                      value: 2,
                                      groupValue: _group,
                                      onChanged: (value) {
                                        setState(() {
                                          _group = value;
                                        });
                                      },
                                    ),
                                    Container(
                                      width: width*0.2,
                                      child: Text(
                                        "Third person",
                                        style: TextStyle(
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            ]),
      ),
    );
  }
}

