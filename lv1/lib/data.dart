class Data {
  String author;
  String year;
  String text;
  String image_url;

  Data({this.text, this.year, this.author, this.image_url});
}