import 'package:flutter/material.dart';
import 'data.dart';

class PersonCard extends StatefulWidget {
  final Data data;
  PersonCard({this.data});

  @override
  _PersonCardState createState() => _PersonCardState();
}

double imgWidth = 90;


class _PersonCardState extends State<PersonCard> {
  void removeImage(double imgWidth) {
    setState(() {
      imgWidth = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width*1;
    double height = MediaQuery.of(context).size.height*0.42;
    double containerWidth = width*0.68;
    if(imgWidth == 0) {
      containerWidth = width*0.90;
    }

    return Card(
      margin: EdgeInsets.fromLTRB(5.0, 0, 0, 20.0),
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 5.0, 10.0, 5.0),
        child: Row(
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  widget.data.image_url = "";
                });
                imgWidth = 0;
                removeImage(imgWidth);
              },
              child: Image.network(
                widget.data.image_url,
                width: imgWidth,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(5.0, 0, 0, 0),
              child: Container(
                width: containerWidth,
                height: height*0.33,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.data.author,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      widget.data.year,
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: SingleChildScrollView(
                        child: Text(
                          widget.data.text,
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
