import 'package:flutter/material.dart';

final controller = PageController(
  initialPage: 0,
);

String text = "";

void main() {
  runApp(MaterialApp(home: MyApp(),));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("LV3"),),
      body: PageView(
      controller: controller,
      children: [
        Page1(),
        Page2(),
        Page3(),
        Page4(),
      ],
      ),
    );
  }
}

class Page1 extends StatefulWidget {
  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 70),
          child: Form(
            key: _formKey,
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    hintText: "Unesi neki tekst.."
                  ),
                  validator: (val) => val.isEmpty ? 'Unesi tekst' : null,
                  onChanged: (val) {
                    if(mounted) {
                      setState(() => text = val);
                    }
                  },
                ),
                SizedBox(height: 20),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  onPressed: () {
                    if(_formKey.currentState.validate()) {
                      controller.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.ease)
                      .then((_) => _formKey.currentState.reset());
                    }
                  },
                  child: Text("Posalji"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Page2 extends StatefulWidget {
  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text,
              style: TextStyle(
                fontSize: 30,
                color: Color.fromRGBO(120, 120, 120, 1),
                fontStyle: FontStyle.italic,
              ),
              ),
              SizedBox(height: 20),
            Text(
              "Jel ovo tvoj tekst?",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  child: Icon(Icons.thumb_up),
                  onTap: () {
                    controller.animateToPage(2, duration: Duration(milliseconds: 500), curve: Curves.ease);
                  },
                ),
                SizedBox(width: 30),
                InkWell(
                  child: Icon(Icons.thumb_down),
                  onTap: () {
                    controller.animateToPage(3, duration: Duration(milliseconds: 500), curve: Curves.ease);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Page3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Yaaaay",
              style: TextStyle(
                fontSize: 40,
                color: Colors.white,
              ),
              ),
            Icon(
              Icons.sentiment_satisfied_alt,
              size: 50,
              color: Colors.white,
              ),
            SizedBox(height: 50),
            InkWell(
              child: Text(
                "Try again?",
                style: TextStyle(
                  fontSize: 20,
                  color: Color.fromRGBO(230, 230, 230, 1), 
                ),
              ),
              onTap: () {
                controller.animateToPage(0, duration: Duration(milliseconds: 500), curve: Curves.ease);
              },
            ),
          ]
        ),
      ),
    );
  }
}

class Page4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Oh nooo",
              style: TextStyle(
                fontSize: 40,
                color: Colors.white,
              ),
              ),
            Icon(
              Icons.sentiment_dissatisfied_outlined,
              size: 50,
              color: Colors.white,
              ),
              SizedBox(height: 50),
            InkWell(
              child: Text(
                "Try again?",
                style: TextStyle(
                  fontSize: 20,
                  color: Color.fromRGBO(230, 230, 230, 1), 
                ),
              ),
              onTap: () {
                controller.animateToPage(0, duration: Duration(milliseconds: 500), curve: Curves.ease);
              },
            ),
          ]
        ),
      ),
    );
  }
}