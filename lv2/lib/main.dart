import 'package:flutter/material.dart';
import 'package:lv2/Student.dart';

void main() {
  runApp(MaterialApp(
    home: Students(),
  ));
}

void addStudent(Student student) {
    students.add(student);
}


List students = [
  Student(firstName: "Ivan", lastName: "Kikic"),
  Student(firstName: "John", lastName: "Doe")
];

class Students extends StatefulWidget {

  @override
  _StudentsState createState() => _StudentsState();
}

class _StudentsState extends State<Students> {

  void removeStudent(Student student) {
    setState(() {
      students.remove(student);
    });
  }

  void refreshState() {
    setState(() {
      print(students.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[700],
        title: Text("List of students"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("NAME"),
                  Text("DELETE"),
                ],
              ),
            ),
            SizedBox(
              height: 0.5,
              child: Container(
                color: Colors.teal[700],
              ),
            ),
            for ( var i in students ) Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                color: Colors.transparent,
                shadowColor: Colors.transparent,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          "${i.toString()}",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {removeStudent(i);},
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        child: FloatingActionButton(
          elevation: 0,
          backgroundColor: Colors.teal[300],
          onPressed: () async {
            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => StudentForm()),
            );
            if(result == "Added") {
              refreshState();
            }
          },
          child: Text("Add", style: TextStyle(color: Colors.white),),
        ),
      )
    );
  }
}

class StudentForm extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controller1 = new TextEditingController();
  final TextEditingController controller2 = new TextEditingController();
  int uniqueID = 0;
  String firstName, lastName;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add new student"),
        backgroundColor: Colors.teal[700],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    hintText: "Please enter first name",
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "First name can't be empty";
                    }
                    firstName = value;
                    return null;
                  },
                  controller: controller1,
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: "Please enter last name",
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "First name can't be empty";
                    }
                    lastName = value;
                    return null;
                  },
                  controller: controller2,
                ),
                SizedBox(
                  height: 20,
                ),
               RaisedButton(
                 onPressed: () {
                   if (_formKey.currentState.validate()) {
                     controller1.text = "";
                     controller2.text = "";
                     Student student = new Student(firstName: firstName, lastName: lastName);
                     addStudent(student);
                     Navigator.pop(context, 'Added');
                   }
                 },
                 color: Colors.teal[300],
                 textColor: Colors.white,
                 child: Text("Add student"),
               )
              ],
            ),
          ),
        ),
      ),
    );
  }
}


