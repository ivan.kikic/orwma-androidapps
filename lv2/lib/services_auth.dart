import 'package:firebase_auth/firebase_auth.dart';
import 'package:jobly_it_jobs_croatia/models/user.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  LocalUser _userFromFirebaseUser(User user) {
    return user != null ? LocalUser(uid: user.uid) : null;
  }

  // Change user
  Stream<LocalUser> get user {
    return _auth.authStateChanges()
        .map(_userFromFirebaseUser);
  }
  // Login

  // Register
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      User user = result.user;
      return _userFromFirebaseUser(user);
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

  // Sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch(e) {
      print(e.toString());
      return null;
    }
  }
}