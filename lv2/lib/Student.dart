class Student {

  String firstName;
  String lastName;

  Student({this.firstName, this.lastName});

  @override
  String toString() {
    return '${this.firstName} ${this.lastName} ';
  }

}