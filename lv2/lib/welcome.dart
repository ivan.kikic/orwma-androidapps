import 'package:flutter/material.dart';
import 'package:jobly_it_jobs_croatia/login.dart';
import 'package:jobly_it_jobs_croatia/register.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 100, 0, 200),
              child: Text("Welcome to Jobly",
                style: TextStyle(
                    fontSize: 25,
                    color: Color.fromRGBO(117, 200, 58, 1),
                    fontFamily: 'Roboto',
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) => LoginScreen())
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 45),
                      child: Text("Log in",
                      style: TextStyle(
                        fontSize: 20,
                        color: Color.fromRGBO(117, 200, 58, 1),
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w300,
                      ),
                      ),
                    ),
                      shape: new RoundedRectangleBorder(side: BorderSide(
                        color: Color.fromRGBO(117, 200, 58, 1),
                      ),
                          borderRadius: new BorderRadius.circular(10.0))
                  ),
                ),
                FlatButton(
                    onPressed: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) => Register())
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 39),
                      child: Text("Sign up",
                        style: TextStyle(
                          fontSize: 20,
                          color: Color.fromRGBO(117, 200, 58, 1),
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                    textColor: Color.fromRGBO(117, 200, 58, 1),
                    shape: RoundedRectangleBorder(side: BorderSide(
                      color: Color.fromRGBO(117, 200, 58, 1),
                      width: 1,
                      style: BorderStyle.solid
                    ),
                        borderRadius: BorderRadius.circular(10.0),
                    )
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
